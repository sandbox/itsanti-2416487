
Module: Hello Block
Author: Aleksandr Kurov <http://www.drupal.org/user/3147109>


Description
===========
Simple test module.

Installation
============
* Copy the 'hello_block' module directory in to your Drupal
sites/all/modules directory as usual.
